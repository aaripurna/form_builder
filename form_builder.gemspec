$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "form_builder/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "form_builder"
  spec.version     = FormBuilder::VERSION
  spec.authors     = ["nawa"]
  spec.email       = ["nap.aripurna@gmail.com"]
  spec.homepage    = "https://gitlab.com/aaripurna/form_builder"
  spec.summary     = "This is a form builder scaffold-like"
  spec.description = "I made this thing myself."
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 6.0.0"

  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency "pry-rails"
end
