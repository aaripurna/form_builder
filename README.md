# FormBuilder
Short description and motivation.
For this moment only working well with ActiveStorage, Postgrsql, Rails >= 5

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'form_builder'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install form_builder
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
