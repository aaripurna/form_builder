require "form_builder/railtie"
require "form_builder/dashboard_form"

module FormBuilder
  # Your code goes here...
  def dashboard_form_for(object, *options, &block)
    args = options.extract_options!
    options << args.merge(builder: DashboardForm)
    form_for(object, *options, &block)
  end

  def present(object, klass = nil)
    return if object.blank?
    klass ||= "#{object.class}Presenter".constantize
    presenter = klass.new(object, self)
    yield presenter if block_given?
    presenter
  end

  def dashboard_objects_path(object)
    self.method("dashboard_#{plural_name(object)}_path").call(object)
  end

  def edit_dashboard_object_path(object)
    self.method("edit_dashboard_#{singular_name(object)}_path").call(object)
  end

  def new_dashboard_object_path(object)
    self.method("new_dashboard_#{singular_name(object)}_path").call(object)
  end

  def dashboard_object_path(object)
    self.method("dashboard_#{singular_name(object)}_path").call(object)
  end

  private

  def singular_name(object)
    object.class.name.underscore
  end

  def plural_name(object)
    singular_name(object).pluralize
  end
end
