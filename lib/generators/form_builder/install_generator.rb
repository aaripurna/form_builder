module FormBuilder
  module Generators
    class InstallGenerator < ::Rails::Generators::Base
      source_root File.expand_path('templates', __dir__)


      def initialize_forom_builder
        inject_into_file Rails.root.join('app/helpers/application_helper.rb'), "\n\tinclude FormBuilder", after: 'module ApplicationHelper'

        template 'layout.haml.erb', Rails.root.join('app/views/layouts/dashboard.html.haml')

        template 'auth.haml.erb', Rails.root.join('app/views/devise/sessions/new.html.haml')
        template 'presenter.erb', Rails.root.join('app/presenters/base_presenter.rb')
        template 'controllers/dashboard.erb', Rails.root.join('app/controllers/dashboard/dashboard_controller.rb')
        template 'controllers/dashboard_api.erb', Rails.root.join('app/controllers/dashboard/api/api_controller.rb')
        template 'dashboard_index.haml.erb', Rails.root.join('app/views/partials/dashboard/_dashboard_index.html.haml')
        template 'javascript.erb', Rails.root.join('app/javascript/packs/dashboard.js')

        inject_into_file Rails.root.join('config/routes.rb'), %{
          concern :detachable do
            member do
              delete :detach
            end
          end

          namespace :dashboard do
            namespace :api, defaults: { format: :json } do

            end
          end

          scope module: :client, shallow: true do
            namespace :api, defaults: { format: :json } do
            end
          end
        }, after: 'Rails.application.routes.draw do'
      end
    end
  end
end
