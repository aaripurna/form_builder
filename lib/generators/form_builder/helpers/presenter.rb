module FormBuilder
  module Generators
    module Helpers
      module Presenter
        include ::ActiveSupport::Concern

        def presenter_name
          "#{singular_name.camelize}Presenter"
        end

        def model_file_name
          "#{singular_name.underscore}_presenter"
        end
      end
    end
  end
end
