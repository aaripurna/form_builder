module FormBuilder
  module Generators
    module Helpers
      module Controller
        include ::ActiveSupport::Concern

        def controller_class_name
          "#{plural_name.camelize}Controller"
        end

        def controller_file_name
          "#{plural_name.underscore}_controller"
        end

        def index_to_show
          if single?
           %Q|@#{singular_name} = #{model_name}.first
              if @#{singular_name}.present?
                redirect_to dashboard_#{singular_name}_path(@#{singular_name})
              else
                redirect_to action: :new
              end
            |
          else
            %Q|@#{plural_name} = #{model_name}.all|
          end
        end

        def setter_to_show
          if single?
            {
              finder: "#{model_name}.first!",
              redirect: "action: :new"
            }
          else
            {
              finder: "#{model_name}.find(params[:id])",
              redirect: "action: :index"
            }
          end
        end

        def attachable_galleries
          galleries = []

          fields.each do |field|
            name, type, arguments = field.split(':')
            row = entity_aliases[type]

            if ['galleries', 'gallery'].include?(type) || row['form'] == 'galleries'
              galleries.push(name)
            end
          end

          galleries
        end

        def params_to_permit
          multi_params = []
          single_params = []
          fields.each do |field|
            name, type, arguments = field.split(':')
            row = entity_aliases[type]

            if row['multiple'].present? || ['array', 'multiple'].include?(arguments)
              multi_params << "#{name}: []"
            else
              single_params << ":#{name}"
            end
          end
          single_params.concat(multi_params)
        end

        def controller_template
          if client_api?
            {
              template: 'contollers/client_api_instance.erb',
              destination: Rails.root.join("app/controllers/client/api/#{controller_file_name}.rb")
            }
          elsif api?
            {
              template: 'controllers/dashboard_api_instance.erb',
              destination: Rails.root.join("app/controllers/dashboard/api/#{controller_file_name}.rb")
            }
          else
             {
              template: 'controllers/dashboard_instance.erb', 
              destination: Rails.root.join("app/controllers/dashboard/#{controller_file_name}.rb")
             } 
          end
              
        end
      end
    end
  end
end
