module FormBuilder
  module Generators
    module Helpers
      module Model
        include ::ActiveSupport::Concern

        def model_name
          singular_name.camelize
        end

        def model_file_name
          singular_name.underscore
        end

        def attachments_to_model
          files = []
          fields.each do |entity|
            name, type, argument = entity.split(':')
            row = entity_aliases[type]
            if row['type'] == 'file' || type == 'file'
              file = (row['multiple'].present? || ['array', 'multiple'].include?(argument)) ? "has_many_attached :#{name}" : "has_one_attached :#{name}"
              files.push file
            end
            if type == 'references' || row['field'] == 'references'
              files.push("belongs_to :#{name}")
            end
          end
          files
        end
      end
    end
  end
end
