module FormBuilder
  module Generators
    module Helpers
      module Validation
        extend ::ActiveSupport::Concern
        
        def validate_file_name
          raise(NameError, 'Invalid file name') if file_name.match(/(^\d)|[^\w]/)
        end

        def validate_field_and_type
          fields.each do |field|
            attributes = field.split(":")
            attributes.each do |attribute|
              raise(NameError, 'Invalid field or type name') if attribute.match(/(^\d)|[^\w]/)
            end
          end
        end

      end
    end
  end
end
