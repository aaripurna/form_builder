require 'yaml'
require 'json'
require 'time'

module FormBuilder
  module Generators
    module Helpers
      module Fields
        include ::ActiveSupport::Concern

        def entity_aliases
          @entity_aliases ||= YAML.load_file(File.expand_path('entity_aliases.yml', __dir__))
        end

        def timestamp
          @timestamp ||= Time.now.strftime("%Y%m%d%H%M%S")
        end

        def single?
          options['scope']&.include?('single')
        end

        def client_api?
          options['scope']&.include?('client-api')
        end

        def api?
          options['scope']&.include?('api')
        end

        def no_view?    
          options['scope']&.include?('no-view')
        end
        
        def generate_dashboard_view?
          !no_view? || client_api?
        end
      end
    end
  end
end
