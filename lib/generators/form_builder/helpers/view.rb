module FormBuilder
  module Generators
    module Helpers
      module View
        include ::ActiveSupport::Concern

        def input_forms
          fields.map do |field|
            name, type, argument = field.split(':')
            label = name
            row = entity_aliases[type]

            if row['field'] == 'references' || type == 'references'
              name = "#{name}_id"
            end
            {name: name, input: row['form'], lable: label}
          end
        end

        def api_view_resource
          valid_scope = {
            dashboard_api: {
              template_root_path: 'views/dashboard_api',
              views: %w{_partial index show},
              destination: Rails.root.join('app/views/dashboard/api/'),
              route: %{\tnamespace :dashboard do\n\t\tnamespace :api, defaults: { format: :json } do},
              controller_path: Rails.root.join('app/controllers/dashboard/api/'),
              controller_template_path: 'controllers/dashboard_api_instance.erb'
            }, client_api: {
              template_root_path: 'views/dashboard/client_api',
              views: [],
              destination: Rails.root.join('app/views/client/api/'),
              route: %{\tscope module: :client, shallow: true do\n\tnamespace :api, defaults: { format: :json } do},
              controller_path: Rails.root.join('app/controllers/client/api/'),
              controller_template_path: 'controllers/client_api_instance.erb'
            }
          }
          if client_api?
            valid_scope[:client_api]
          elsif api?
            valid_scope[:dashboard_api]
          end 
        end

        def dashboard_view_files
          single? %w(new edit _form) : %w(index new edit _form)
        end

        def dashboard_view_path(file)
          Rails.root.join("app/views/dashboard/#{plural_name.underscore}/#{file}")
        end
      end
    end
  end
end
