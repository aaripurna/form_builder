module FormBuilder
  module Generators
    module Helpers
      module Migration 
        include ::ActiveSupport::Concern

        def migration_class_name
          "Create#{model_name.pluralize}" 
        end

        def timestamp
          Time.now.strftime("%Y%m%d%H%M%S")
        end

        def migration_file_name
          "#{timestamp}_create_#{plural_name}".underscore
        end

        def table_name
          plural_name.underscore
        end

        def migration_attributes
          fields.map do |field|
            name, type, argument = field.split(':')
            options = []
            row = entity_aliases[type]
            
            next if ( ['file', 'image', 'galleries'].include?(argument) || row['field'].blank?)

            if (type == 'references' || row['field'] == 'references') && argument&.include?('polymorphic')
              options << "polymorphic: true" 

            elsif type == 'references' || row['field'] == 'references' 
              options << "index: true"
              options << "foreign_key: true"

            elsif argument&.include? ('index')
              options << "index: true" 
            end

            options << "array: true" if (row['multiple'].present? || ['multiple', 'array'].include?(argument))
            
            if argument.present?
              options << "unique: true" if (argument.include?('unique') && !(type == 'references' || row['field'] == 'references'))
            end
            {name: name, field: row['field'], options: options}
          end
        end
      end
    end
  end
end
