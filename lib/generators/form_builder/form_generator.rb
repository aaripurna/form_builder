require File.expand_path('helpers.rb', __dir__)

module FormBuilder
  module Generators 
    class FormGenerator < ::Rails::Generators::NamedBase
      include Helpers::Validation
      include Helpers::Fields
      include Helpers::Controller
      include Helpers::View
      include Helpers::Model
      include Helpers::Presenter
      include Helpers::Migration

      source_root File.expand_path('templates', __dir__)

      class_option :scope, type: :array, default: []
      class_option :module, type: :string
      argument :fields, type: :array, default: []

      def validates_arguments
        validate_file_name
        validate_field_and_type
      end

      def generate_views
        return unless generate_dashboard_view?
        if api_view_resource.present?
          api_view_resource[:views].each do |view|
            template "#{api_view_resource[:template_root_path]}/#{view}.erb", "#{api_view_resource[:destination]}/#{view}.json.jbuilder"
          end
          template "#{api_view_resource[:controller_template_path]}.erb", "#{api_view_resource[:controller_path]}/#{controller_file_name}.rb"
        else
          dashboard_view_files.each do |file|
            template "views/#{file}.erb", "#{dashboard_view_path(file)}.html.haml"
          end

          template controller_template[:template], controller_template[:destination]
        end
      end

      def generate_additional_resources
        template "migration.erb", Rails.root.join("db/migrate/#{migration_file_name}.rb")
        template "model.erb", Rails.root.join("models/#{model_name}.rb")

        # append route
        parent_route = ""
        child_route = "\n\t\t\tresources :#{plural_name.underscore}"
        if api_view_resource.present?
          parent_route = api_view_resource[:route] 
        else
          parent_route = 'namesapace :dashboard do'
        end

        inject_into_file Rails.root.join('config/routes.rb'), child_route, after: parent_route
      end
    end
  end
end
