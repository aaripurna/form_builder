require File.expand_path('basic_fields', __dir__)
require File.expand_path('tag_helpers', __dir__)

module FormBuilder
  class DashboardForm < ::ActionView::Helpers::FormBuilder
    include ::ActiveSupport::Concern
    include FormBuilder::TagHelpers
    include FormBuilder::BasicFields

    class InvalidFieldStructure < StandardError; end

    def text(attribute, args={})
      args[:inputs] = {} if !args[:inputs].is_a?(Hash)
      args[:container] = {} if !args[:container].is_a?(Hash)

      @template.content_tag(:div, {class: 'text-field', **args[:container]}) do
        @template.text_field_tag(set_name(attribute), saved_value(attribute), args[:inputs])
      end
    end

    def form_block(args={}, &block)
      @template.content_tag(:div, {class: 'form-block', **args}) do
        @template.capture(&block)
      end
    end

    def label(attribute, args={}, &block)
      @template.content_tag(:div, {class: 'form-label'}) do
        @template.label_tag(attribute, attribute.to_s.titleize) + 
        @template.content_tag(:span, {class: 'form-label-block'}) do
          @template.capture(&block) if block_given?
        end
      end
    end

    def image_field(attribute, args={})
      image = @template.url_for(saved_value(attribute)) if saved_value(attribute).attached?
      style = "display: #{image.blank? ? 'none' : 'block'}; background-url(#{image if image.present?})"
      id = "file-input-#{set_name(attribute).parameterize}"

      @template.content_tag(:div, {class: 'image-field'}) do
        @template.content_tag(:div, {class: 'image-thumbnail', style: style}) do
          @template.content_tag(:div, {class: 'delete-image'})
        end +
        @template.file_field_tag(set_name(attribute), {style: "display: none; id: #{id}"}) + 
        @template.label_tag(id, {style: "display: #{'none' if image.present?  }"}) do
          @template.content_tag(:div,'', {class: 'add-image'})
        end
      end
    end

    def text_array(attribute, args={})
      values = saved_value(attribute) || ['']
      field_name = set_name(attribute, multiple: true)

      @template.content_tag(:div, {class: 'text-array'}) do
        @template.content_tag(:div, {class: 'text-input'}) do
          @template.content_tag(:div, {class: 'text-input-box'}) do
            values.each_with_index do |value, index|
              @template.concat(@template.text_field_tag(field_name, value) + 
              (@template.content_tag(:i, '', {class: 'remove-button fas fa-trash'}) if !index.zero?))
            end   
          end
        end + 
        @template.content_tag(:div, '', {class: 'add-button fas fa-plus'})
      end
    end

    def textarea(attribute, args={})
      args[:inputs] = {} if !args[:inputs].is_a?(Hash)
      args[:container] = {} if !args[:container].is_a?(Hash)

      @template.content_tag(:div, {class: 'textarea-field', **args[:container]}) do
        @template.text_area_tag(set_name(attribute), saved_value(attribute), args[:inputs]) 
      end
    end

    def slug(attribute, args={})
      if args[:target].blank?
        raise InvalidFieldStructure, "Slug Field need a target. Please specif target e.g. target: :target_field. If your slug don't have a target use basic_slug instead"
      end
      if set_name(args[:target]) == set_name(attribute)
        raise InvalidFieldStructure, "The target can't be the field itself"
      end

      args[:container] ||= {}
      args[:inputs] ||= {}

      args[:inputs].merge!(data: {target: set_name(args[:target])})
      basic_slug(attribute, args)
    end

    def select(attribute, options, args={})
      @template.content_tag(:div, {class: 'dash-selector-field', **args.except(:multiple)}) do
        @template.select_tag(set_name(attribute, multiple: args[:multiple]),
          @template.options_for_select(options, saved_value(attribute)),
          multiple: args[:multiple], class: 'dash-selector-select')
      end
    end

    def currency(attribute, args={})
      value = saved_value(attribute)
      @template.content_tag(:div, {class: 'currency-field'}) do
        @template.content_tag(:span, args[:currency] || 'Rp', class: 'currency-tag') +
        @template.text_field_tag('',
                  @template.number_with_delimiter(value, delimiter: '.'),
                  {class: 'shown-currency'}) +
        @template.hidden_field_tag(set_name(attribute), value)
      end
    end

    ## Only for boolean value
    def toggle_ajax(attribute, args={})
      label1, label2 = args[:labels].split(',')
      label = saved_value(attribute) ? label1 : label2

      @template.content_tag(:div, {class: 'toggle-ajax',
          data: {action: @template.action_name,
          attribute: attribute, object: @object_name,
          messages: args[:messages], labels: args[:labels]
        }}) do
        @template.check_box_tag(attribute, saved_value(attribute), saved_value(attribute)) +
        (@template.hidden_field_tag(set_name(attribute), saved_value(attribute)) if @template.action_name != 'edit') +
        @template.label_tag(attribute, label)
      end
    end

    def single_boolean(attribute, args={})
      id = set_name(attribute).parameterize.underscore
      label = args[:label] || 'Yes'
      @template.content_tag(:div, {class: 'single-boolean'}) do
        @template.check_box_tag(id, '', {checked: saved_value(attribute).present?}) + 
        @template.hidden_field_tag(set_name(attribute), saved_value(attribute).to_s) + 
        @template.label_tag(id, '', {class: :checkmark}) +
        @template.label_tag(id, label)
      end
    end

    def blog_editor(attribute, args={})
      basic_text_editor(attribute, {data: {type: 'blog'}, **args})
    end

    def galleries(attribute, args={})
      gallery_datas = @object.method(attribute).call
      blob_ids = gallery_datas.map { |g| g.blob.id }
      @template.tag.div({id: set_name(attribute).parameterize,
          class: 'galleries-field dropzone', data: { galleries: blob_ids.join(',') } })
    end

    def date(attribute, args={})
      
    end

    def date_range(start_attr, end_attr, args={})
      start_date = @object.method(start_attr).call
      end_date = @object.method(end_attr).call

      @template.content_tag(:div, {class: 'date-range-field'}) do
        @template.content_tag(:div, {class: 'date-range-input'}) do
          @template.text_field_tag(:date, '',{class: 'readonly-input', readonly: true})
        end +
        @template.content_tag(:div, {class: 'date-range-value'}) do
          @template.hidden_field_tag(start_attr, start_date, {class: 'date-range-start'})
          @template.hidden_field_tag(end_attr, end_date, {class: 'date-range-end'})
        end
      end
    end

    def submit
      @template.content_tag(:div, {class: "form-submit auto-submit"}) do
        super
      end
    end

    # aliases goes here
    alias_method :textarray, :text_array
  end
end
