module FormBuilder
  module BasicFields
    include ::ActiveSupport::Concern

    def basic_slug(attribute, args={})
      args[:inputs] = {} if !args[:inputs].is_a?(Hash) 
      args[:container] = {} if !args[:container].is_a?(Hash)
      @template.content_tag(:div, {class: 'slug-field', **args[:container]}) do
        @template.text_field_tag(set_name(attribute), saved_value(attribute), args[:inputs])
      end
    end

    def basic_select(attribute, options, args = {})
      @template.content_tag(:div, {class: 'select-field'}) do
        @template.select_tag(set_name(attribute), @template.options_for_select(options, saved_value(attribute)))
      end
    end

    def basic_text_editor(attribute, args={})
      field_name = set_name(attribute)
      @template.content_tag(:div, {class: 'text-editor', **args}) do
        @template.text_area_tag(field_name, saved_value(attribute), {style: "display: none;", id: field_name.parameterize}) +
        @template.content_tag(:div, '', {input: field_name.parameterize, class: 'text-editor-body'})
      end
    end
  end
end
