module FormBuilder
  module TagHelpers
    include ::ActiveSupport::Concern
    
    def saved_value(attribute)
      @object.read_attribute(attribute)
    end

    def set_name(attribute, options={})
      multiple = '[]' if options[:multiple]
      "#{@object_name}[#{attribute}]#{multiple}"
    end
  end
end
